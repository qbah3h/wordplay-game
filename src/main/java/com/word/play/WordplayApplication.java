package com.word.play;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WordplayApplication {

	public static void main(String[] args) {
		SpringApplication.run(WordplayApplication.class, args);
	}

}
